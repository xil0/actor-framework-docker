FROM alpine:3.12.0

RUN apk update && apk upgrade

RUN apk add cmake \
  g++ \
  git \
  libressl-dev \
  make

RUN apk add linux-headers

ENV CXX=/usr/bin/g++